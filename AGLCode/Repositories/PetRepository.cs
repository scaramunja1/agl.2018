﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;

namespace AGLCode
{
    public class PetRepository : IPetRepository
    {
        public List<Models.PetOwnerModel> GetPets(Definitions.Pets petType)
        {
            var returnModel = new List<Models.PetOwnerModel>();
            GetPetList(ref returnModel);

            if (returnModel.Any(a => a.pets == null))
                { returnModel.RemoveAll(a => a.pets == null); }

            return returnModel.Where(a => a.pets.Any(c => c.type == petType.ToString())).ToList();
        }


        private void GetPetList(ref List<Models.PetOwnerModel> returnModel)
            {
                using (var client = new HttpClient())
                {
                    client.BaseAddress = new Uri("http://agl-developer-test.azurewebsites.net/");
                    client.DefaultRequestHeaders.Accept.Clear();
                    client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
                    HttpResponseMessage response = client.GetAsync("people.json").Result;

                    if (response.IsSuccessStatusCode)
                    {
                        var ownerJson = response.Content.ReadAsStringAsync();
                    // try deserialize:
                    returnModel = Newtonsoft.Json.JsonConvert.DeserializeObject<List<Models.PetOwnerModel>>(ownerJson.Result);
                     }
                    else
                    {
                        throw new Exception(DateTime.Now.ToString() + ", FAILURE: Could not retreive kitty data! Sorry! We'd purrrfectly love to help you later!");
                    }

                   
                }

            }

    }

    public interface IPetRepository
    {
        List<Models.PetOwnerModel> GetPets(Definitions.Pets petType);
    }
}
