﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AGLCode.Models
{
    public class PetOwnerViewModel
    {
        public string ErrorMessage { get; set; }
        public List<PetOwnerModel> PetOwners { get; set; }

    }

    public class PetOwnerModel
    {
        [Required]
        public string name { get; set; }

        [Required]
        public string gender { get; set; }

        public int age { get; set; }

        public List<PetModel> pets { get; set; }
    }

    public class PetModel
    {
        [Required]
        public string name { get; set; }

        [Required]
        public string type { get; set; }

    }
}
