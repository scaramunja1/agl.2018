﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AGLCode
{
    public class CatService : ICatService
    {
        private IPetRepository _petRepository;
        public CatService(IPetRepository petRepository)
        {
            _petRepository = petRepository;
        }

        public bool GetCatsByGenderOrder(Models.PetOwnerViewModel viewModel)
        {
            
            try
                {
                    var serviceModelList = _petRepository.GetPets(Definitions.Pets.Cat);
                    // 1. Do the specific view work:
                    viewModel.PetOwners = serviceModelList.OrderBy(o => o.gender).ThenBy(o => o.pets.OrderBy(c => c.name).First().name).ToList();
                    // 2. Since we are only interested in the cats of the owners, we will remove non-purrfect pets:
                    viewModel.PetOwners.ToList().ForEach(o => RemoveExcept(o.pets, Definitions.Pets.Cat));
                }
            catch (Exception ex)
                {
                    viewModel.ErrorMessage = ex.Message;
                    return false;
                }

            return true;
        }

        private void RemoveExcept(List<Models.PetModel> petList, Definitions.Pets petTypeToRetain)
        {
            List<Models.PetModel> clonedList = new List<Models.PetModel>(petList);
            foreach (var pet in clonedList)
                { if (pet.type != petTypeToRetain.ToString()) { petList.Remove(pet); } }
        }
    }



    public interface ICatService
    {
        bool GetCatsByGenderOrder(Models.PetOwnerViewModel viewModel);
    }
}
