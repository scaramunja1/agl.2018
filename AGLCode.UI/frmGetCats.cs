﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AGLCode.UI
{
    public partial class frmGetCats : Form
    {
        public frmGetCats()
        {
            InitializeComponent();
        }

        private void btnDisplayCats_Click(object sender, EventArgs e)
        {
            // do a try catch or alter the model to be a service model (for later, not requested here)
            // also use Unity to initialise and manage these instances / interfaces... too much trouble for here:
            var petService = new AGLCode.CatService(new PetRepository());

            var viewModel = new Models.PetOwnerViewModel();
            if (!petService.GetCatsByGenderOrder(viewModel))
            {
                txtMessageLog.Text = viewModel.ErrorMessage;
            }

            if (!RenderCatList(viewModel.PetOwners))
            {
                txtMessageLog.Text = DateTime.Now.ToString() + ", FAILURE: One of the pets returned was not a cat. We'll investigate this and get back to you purrrfectly.";
            }

            txtMessageLog.Text = DateTime.Now.ToString() + ", PURRRFECT!: Successfully retreived the catlist and displayed by owner gender and cat name. \n" + txtMessageLog.Text;
        }

        private bool RenderCatList(List<Models.PetOwnerModel> ownersWithPets)
        {
            rtbCatResults.Clear();
            foreach (var catOwner in ownersWithPets)
            {
                if (catOwner == ownersWithPets.FirstOrDefault(o => o.gender == catOwner.gender))
                {
                    rtbCatResults.SelectionFont = new Font("Georgia", 16, FontStyle.Bold);
                    rtbCatResults.SelectedText = catOwner.gender.ToString() + "\n";
                }

                foreach (var cat in catOwner.pets)
                {
                    if (cat.type != "Cat")             { return false; }
                    rtbCatResults.SelectionBullet = true;
                    rtbCatResults.SelectionFont = new Font("Georgia", 12);
                    rtbCatResults.SelectedText = cat.name.ToString() + "\n";
                    rtbCatResults.SelectionBullet = false;
                }
            }
            return true;
        }
    }
}
