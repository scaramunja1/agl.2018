﻿namespace AGLCode.UI
{
    partial class frmGetCats
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnDisplayCats = new System.Windows.Forms.Button();
            this.txtMessageLog = new System.Windows.Forms.RichTextBox();
            this.lblMessageLog = new System.Windows.Forms.Label();
            this.rtbCatResults = new System.Windows.Forms.RichTextBox();
            this.SuspendLayout();
            // 
            // btnDisplayCats
            // 
            this.btnDisplayCats.FlatAppearance.BorderColor = System.Drawing.Color.Blue;
            this.btnDisplayCats.FlatAppearance.BorderSize = 2;
            this.btnDisplayCats.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.btnDisplayCats.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(128)))));
            this.btnDisplayCats.FlatStyle = System.Windows.Forms.FlatStyle.Popup;
            this.btnDisplayCats.ForeColor = System.Drawing.SystemColors.MenuHighlight;
            this.btnDisplayCats.Location = new System.Drawing.Point(12, 12);
            this.btnDisplayCats.Name = "btnDisplayCats";
            this.btnDisplayCats.Size = new System.Drawing.Size(310, 23);
            this.btnDisplayCats.TabIndex = 0;
            this.btnDisplayCats.Text = "Push this button to display the Cat List";
            this.btnDisplayCats.UseVisualStyleBackColor = true;
            this.btnDisplayCats.Click += new System.EventHandler(this.btnDisplayCats_Click);
            // 
            // txtMessageLog
            // 
            this.txtMessageLog.Location = new System.Drawing.Point(366, 41);
            this.txtMessageLog.Name = "txtMessageLog";
            this.txtMessageLog.Size = new System.Drawing.Size(297, 331);
            this.txtMessageLog.TabIndex = 4;
            this.txtMessageLog.Text = "";
            // 
            // lblMessageLog
            // 
            this.lblMessageLog.AutoSize = true;
            this.lblMessageLog.Location = new System.Drawing.Point(363, 17);
            this.lblMessageLog.Name = "lblMessageLog";
            this.lblMessageLog.Size = new System.Drawing.Size(74, 13);
            this.lblMessageLog.TabIndex = 5;
            this.lblMessageLog.Text = "Message Log:";
            // 
            // rtbCatResults
            // 
            this.rtbCatResults.Location = new System.Drawing.Point(12, 41);
            this.rtbCatResults.Name = "rtbCatResults";
            this.rtbCatResults.Size = new System.Drawing.Size(310, 331);
            this.rtbCatResults.TabIndex = 6;
            this.rtbCatResults.Text = "";
            // 
            // frmGetCats
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(687, 387);
            this.Controls.Add(this.rtbCatResults);
            this.Controls.Add(this.lblMessageLog);
            this.Controls.Add(this.txtMessageLog);
            this.Controls.Add(this.btnDisplayCats);
            this.Name = "frmGetCats";
            this.Text = "John Sumich - AGL Code Test - \"Show Me The Cats\"";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btnDisplayCats;
        private System.Windows.Forms.RichTextBox txtMessageLog;
        private System.Windows.Forms.Label lblMessageLog;
        private System.Windows.Forms.RichTextBox rtbCatResults;
    }
}

