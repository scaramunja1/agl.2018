﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using NSubstitute;

namespace AGLCode.Test
{
    [TestClass]
    public class ServiceTests
    {
        [TestMethod]
        public void AnyRepositoryErrorGeneratesFailureAndUIError()
        {
            // Arrange:
            var IPetRepository = Substitute.For<IPetRepository>();
            IPetRepository.GetPets(Definitions.Pets.Cat).Returns(x => { throw new Exception(); });

            // Act:
            var service = new CatService(IPetRepository);
            var modelResult = new Models.PetOwnerViewModel();
            var result = service.GetCatsByGenderOrder(modelResult);

            // Assert:
            Assert.AreEqual(result, false);
            Assert.AreEqual(string.IsNullOrEmpty(modelResult.ErrorMessage.ToString()), false);
            
        }

       

    }
}
